//
//  Basic_MVVM_SwiftUI_NavigationApp.swift
//  Basic_MVVM_SwiftUI_Navigation
//
//  Created by Sumit Vishwakarma on 17/05/23.
//

import SwiftUI

@main
struct Basic_MVVM_SwiftUI_NavigationApp: App {
    var body: some Scene {
        WindowGroup {
            FirstView()
        }
    }
}
