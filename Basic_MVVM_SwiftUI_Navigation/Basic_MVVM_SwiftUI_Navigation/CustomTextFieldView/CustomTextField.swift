
import SwiftUI
import Combine



//MARK: CustomTextField
struct CustomTextField: View {
    var isSecureTextField: Bool
    @ObservedObject var firstVM: FirstViewModel
    
    var body: some View {
        VStack {
            if isSecureTextField {
                securePasswordTextfield
            } else {
                normalTextField
            }
        }
    }
}

//MARK: CustomTextField Extension
private extension CustomTextField {
    
    var securePasswordTextfield: some View {
        SecureField("Password", text: self.$firstVM.password)
            .padding(20)
            .frame(width: UIScreen.main.bounds.width - 50, height: 55)
            .border(Color.init(Color.RGBColorSpace.sRGB,red: 1.0,green: 0.2,blue: 0.4,opacity: 1.0))
            .background(Color.white)
            .padding([.leading, .trailing], 35)
            .cornerRadius(10)
     }
    
    var normalTextField: some View {
        TextField("Username", text: self.$firstVM.userName)
            .padding(20)
            .frame(width: UIScreen.main.bounds.width - 50, height: 55)
            .border(Color.init(Color.RGBColorSpace.sRGB,red: 1.0,green: 0.2,blue: 0.4,opacity: 1.0))
            .background(Color.white)
            .padding([.leading, .trailing], 35)
            .cornerRadius(10)
     }
}
