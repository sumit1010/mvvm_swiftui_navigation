
import SwiftUI
import Combine

struct FirstView: View {
    
    @ObservedObject private var firstVM = FirstViewModel()
    
    var body: some View {
        NavigationView {
            VStack(spacing: 20) {
                
                NavigationLink("", destination: SecondView(), isActive: self.$firstVM.isLoginSuccess)
                Text("MVVM SwiftUI Navigation")
                
                //MARK: Username And Password TextField
                CustomTextField(isSecureTextField: false, firstVM: firstVM)
                CustomTextField(isSecureTextField: true,  firstVM: firstVM)
                
                //MARK: Login Button
                LoginButton
                    .alert(isPresented: $firstVM.isShowAlert) {
                        Alert(title: Text(self.firstVM.validation()), message: Text(""), dismissButton: .default(Text("Ok")))
                    }
            }
            .navigationViewStyle(StackNavigationViewStyle())
        }
    }
}

//MARK: LoginView Extension
private extension FirstView {
    
    var LoginButton: some View {
        Button("Login") {
            DispatchQueue.main.async {
                firstVM.isShowAlert = self.firstVM.validation() != "" ? true : false
                if firstVM.isShowAlert == false {
                    self.firstVM.activeNavigation()
                }
            }
        }
        .frame(width: UIScreen.main.bounds.width - 50, height: 55)
        .background(Color.yellow)
        .cornerRadius(20)
    }
}

