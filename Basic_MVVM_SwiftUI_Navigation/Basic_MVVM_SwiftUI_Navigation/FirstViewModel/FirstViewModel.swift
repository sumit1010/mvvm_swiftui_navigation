
import SwiftUI
import Combine

class FirstViewModel: ObservableObject {
    @Published var isLoginSuccess = false
    @Published var userName = ""
    @Published var password = ""
    @Published var isShowAlert: Bool = false
    
    func activeNavigation() {
        self.isLoginSuccess = true
    }
    
    //MARK:  TextFields Validation Method
    func validation() -> String {
        if self.userName == "" {
            return "Enter username"
        }
        if self.password == "" {
            return "Enter password"
        }
        return ""
    }
}
